package test.modele;

import org.junit.Assert;
import org.junit.Test;

import modele.SaisieDeSixChiffres;

public class TestSaisieDeSixChiffres {

    @Test
    public void saisieDeSixChiffres_casLongueurDifferenteDeSix() {
        SaisieDeSixChiffres saisie = new SaisieDeSixChiffres("1234567");
        try {
            saisie.verifierSaisie("1234567");
        } catch (NumberFormatException e) {
            Assert.assertEquals("Tailles de la saisie differente de six", e.getMessage());
        }
    }

    @Test
    public void saisieDeSixChiffres_casSaisieContientPasQueChiffre() {
        SaisieDeSixChiffres saisie = new SaisieDeSixChiffres("1a2e3z");
        try {
            saisie.verifierSaisie("1a2e3z");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("La saisie ne contient pas que des chiffres", e.getMessage());
        }
    }
}
