package test.controller;

import org.junit.Test;

import controller.HeuresPossibles;
import junit.framework.Assert;

public class TestHeuresPossibles {

    @Test
    public void testerVerfierPossibilite_casNonValide() {
        String possibilite = "987654";
        HeuresPossibles heures = new HeuresPossibles();
        Assert.assertFalse(heures.verifierPossibilite(possibilite));
    }

    @Test
    public void testerVerfierHeure_casNonValide() {
        String possibilite = "987654";
        HeuresPossibles heures = new HeuresPossibles();
        Assert.assertFalse(heures.verifierHeure(possibilite));
    }

    @Test
    public void testerVerfierMinute_casNonValide() {
        String possibilite = "987654";
        HeuresPossibles heures = new HeuresPossibles();
        Assert.assertFalse(heures.verifierMinute(possibilite));
    }

    @Test
    public void testerVerfierSeconde_casNonValide() {
        String possibilite = "987664";
        HeuresPossibles heures = new HeuresPossibles();
        Assert.assertFalse(heures.verifierSeconde(possibilite));
    }

    @Test
    public void testerVerfierPossibilite_casValide() {
        String possibilite = "123456";
        HeuresPossibles heures = new HeuresPossibles();
        Assert.assertTrue(heures.verifierPossibilite(possibilite));
    }

    @Test
    public void testerVerfierHeure_casValide() {
        String possibilite = "123456";
        HeuresPossibles heures = new HeuresPossibles();
        Assert.assertTrue(heures.verifierHeure(possibilite));
    }

    @Test
    public void testerVerfierMinute_casValide() {
        String possibilite = "123456";
        HeuresPossibles heures = new HeuresPossibles();
        Assert.assertTrue(heures.verifierMinute(possibilite));
    }

    @Test
    public void testerVerfierSeconde_casValide() {
        String possibilite = "123456";
        HeuresPossibles heures = new HeuresPossibles();
        Assert.assertTrue(heures.verifierSeconde(possibilite));
    }
}