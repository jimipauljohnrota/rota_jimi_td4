package controller;

import java.util.HashSet;
import java.util.Set;

public class HeuresPossibles {
    private Set<String> heuresPossibles = new HashSet<String>();
    int heure;
    int minute;
    int seconde;

    public HeuresPossibles() {
    }

    public HeuresPossibles(Set<String> possibilites) {
        boolean resultat = false;
        if (possibilites.size() != 0) {
            for (String possibilite : possibilites) {
                resultat = verifierPossibilite(possibilite);
            }
        } else if (resultat == false) {
            throw new IllegalArgumentException("Aucune heure est possible avec cette saisie");
        }
    }

    public boolean verifierPossibilite(String possibilite) {
        if (verifierHeure(possibilite) && verifierMinute(possibilite) && (verifierSeconde(possibilite))) {
            String heure = adapterString(possibilite);
            heuresPossibles.add(heure);
            return true;
        }
        return false;
    }

    public boolean verifierHeure(String possibilite) {
        if (((Character.getNumericValue(possibilite.charAt(0)) == 2)
                && (Character.getNumericValue(possibilite.charAt(1)) < 4))
                || (Character.getNumericValue(possibilite.charAt(0)) < 2)) {
            int heure = (Character.getNumericValue(possibilite.charAt(0)) * 10)
                    + Character.getNumericValue(possibilite.charAt(1));
            this.heure = heure;
            return true;
        }
        return false;
    }

    public boolean verifierMinute(String possibilite) {
        if (((Character.getNumericValue(possibilite.charAt(2)) < 6))) {
            int minute = (Character.getNumericValue(possibilite.charAt(2)) * 10)
                    + Character.getNumericValue(possibilite.charAt(3));
            this.minute = minute;
            return true;
        }
        return false;
    }

    public boolean verifierSeconde(String possibilite) {
        if (((Character.getNumericValue(possibilite.charAt(4)) < 6))) {
            int seconde = (Character.getNumericValue(possibilite.charAt(4)) * 10)
                    + Character.getNumericValue(possibilite.charAt(5));
            this.seconde = seconde;
            return true;
        }
        return false;
    }

    private String adapterString(String stringAccepte) {
        String resultat = heure + "H" + minute + "min" + seconde + "s";
        return resultat;
    }

    public Set<String> getHeuresPossibles() {
        return heuresPossibles;
    }

    public void afficherHeuresPossibles() {
        for (String heurepossible : heuresPossibles) {
            System.out.println(heurepossible);
        }
    }
}
