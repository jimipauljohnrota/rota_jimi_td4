package controller;

import java.util.HashSet;
import java.util.Set;

public class Permutation {

    private Set<String> possibilites = new HashSet<String>();

    public Permutation(String str) {
        possibilites.addAll(genererPermutation(str));
    }

    public Set<String> genererPermutation(String conversionSaisie) {
        Set<String> permutation = new HashSet<String>();
        if (conversionSaisie == null) {
            return null;
        } else if (conversionSaisie.length() == 0) {
            permutation.add("");
            return permutation;
        }
        char initial = conversionSaisie.charAt(0);
        String rem = conversionSaisie.substring(1);
        Set<String> string = genererPermutation(rem);
        for (String stringGenere : string) {
            for (int i = 0; i <= stringGenere.length(); i++) {
                permutation.add(insertionCaractere(stringGenere, initial, i));
            }
        }
        return permutation;
    }

    public String insertionCaractere(String chaine, char caractere, int j) {
        String begin = chaine.substring(0, j);
        String end = chaine.substring(j);
        return begin + caractere + end;
    }

    public Set<String> getPossibilites() {
        return possibilites;
    }

    public Set<String> setPossibilites(String possibilite) {
        possibilites.add(possibilite);
        return possibilites;
    }
}