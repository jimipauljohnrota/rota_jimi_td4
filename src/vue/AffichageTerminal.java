package vue;

import controller.HeuresPossibles;
import controller.Permutation;
import modele.SaisieDeSixChiffres;

public class AffichageTerminal {
    public AffichageTerminal() {
        boolean valide = true;
        SaisieDeSixChiffres saisie = null;
        do {
            valide = true;
            try {
                saisie = new SaisieDeSixChiffres();
            } catch (Exception e) {
                valide = false;
            }
        } while (valide == false);
        Permutation permutation = new Permutation(saisie.getSaisieSixChiffres());
        HeuresPossibles heures = new HeuresPossibles(permutation.getPossibilites());
        heures.afficherHeuresPossibles();
    }
}
