package modele;

import java.util.Scanner;

public class SaisieDeSixChiffres {

    private String saisieSixChiffres = "";
    private String chiffres = "";

    public SaisieDeSixChiffres(String chiffres) {
        this.chiffres = chiffres;
    }

    public SaisieDeSixChiffres() {
        System.out.println("Veuillez saisir 6 chiffres");
        Scanner saisieClavier = new Scanner(System.in);
        chiffres = saisieClavier.next();

        verifierSaisie(chiffres);
        this.saisieSixChiffres = chiffres;
        saisieClavier.close();
    }

    public String getSaisieSixChiffres() {
        return saisieSixChiffres;
    }

    public void setSaisieSixChiffres(String saisieSixChiffres) {
        this.saisieSixChiffres = saisieSixChiffres;
    }

    public String getChiffres() {
        return chiffres;
    }

    public void setChiffres(String chiffres) {
        this.chiffres = chiffres;
    }

    public void verifierSaisie(String chiffres) {
        if (!(chiffres.matches("[0-9]+"))) {
            throw new IllegalArgumentException("La saisie ne contient pas que des chiffres");
        } else if ((chiffres.length() > 6) || (chiffres.length() < 6)) {
            throw new NumberFormatException("Tailles de la saisie differente de six");
        }
    }
}
